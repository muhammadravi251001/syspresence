# syspresence

## TODO (aplikasi)
Sisa TODO-nya implementasi services Student, yaitu:
* Tambah student ke suatu Course
* Read student dari suatu Course
* Update student dari suatu Course
* Hapus student dari suatu Course
* Tambahin absensi ke suatu student

## TODO (cloud)
* Pecah service-service tersebut jadi microservices
* Urusin semua hal tentang cloudnya

## Ide dan Rencana

Jadi, rencananya adalah kita buat aplikasi microservice gitu, yang baru kepikiran sih ya dua service ya:
* Service homepage & melakukan absensi -- app home
* Service menambahkan matkul dan siswanya -- app course

Biar ga terlalu ribet, buat 2 service aja.

Inti dari TK kowan ini, gimana cara memaksimalkan kinerja aplikasi dengan bantuan cloud. Yang baru kepikiran sama gua adalah: microservice.