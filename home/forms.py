from django import forms
from .models import *

class CourseForm(forms.ModelForm):
	class Meta:
		model = Course
		fields = '__all__'

class StudentForm(forms.ModelForm):
	class Meta:
		model = Student
		fields = '__all__'
	'''
	student_id = models.AutoField(primary_key=True)
	student_name = models.CharField(max_length=100)
	absence_number = models.CharField(max_length=100)

	course_taken = forms.ModelMultipleChoiceField(queryset=Course.objects.all())
	'''
	#course_taken = forms.ModelMultipleChoiceField(queryset=Course.objects.all(), \
	#	widget=forms.CheckboxSelectMultiple)