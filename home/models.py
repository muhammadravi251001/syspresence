from django.db import models

class Course(models.Model):

    # Model untuk menambahkan course (mata kuliah)
    course_id = models.AutoField(primary_key=True, unique=True)
    
    course_name = models.CharField(max_length=100)
    teacher_name = models.CharField(max_length=100)
    course_description = models.CharField(max_length=100)
    class_location = models.CharField(max_length=100)

    def __str__(self):
        return self.course_name

    #TODO: gua yakin alesannya karena id nya ga diambil, malah diambil name
    def __call__(self):
        return self.course_id

class Student(models.Model):

    # Model untuk menambahkan course dari POV siswa
    course_taken = models.ManyToManyField(Course)
    
    # Model untuk menambahkan siswa ke suatu course
    student_id = models.AutoField(primary_key=True)
    student_name = models.CharField(max_length=100)
    absence_number = models.CharField(max_length=100)

    '''
    # Model untuk menambahkan keterangan absensi siswa
    is_present = models.BooleanField(default=True)
    is_permit = models.BooleanField(default=False)
    is_alpha = models.BooleanField(default=False)
    '''

    def __str__(self):
        return self.student_name