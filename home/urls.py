from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.PageView.landing_page, name='landing_page'),
    path('add_course_page/', views.PageView.add_course_page, name='add_course_page'),
    path('all_course_page/', views.PageView.all_course, name='all_course_page'),
    path('add_student_page/', views.PageView.add_student_page, name='add_student_page'),

    path('add_course/', views.CourseView.add_course, name='add_course'),
    path('update_course/<str:pk>/', views.CourseView.update_course, name='update_course'),
    path('delete_course/<str:pk>/', views.CourseView.delete_course, name='delete_course'),
    path('detail_course_page/<str:pk>/', views.CourseView.detail_course, name='detail_course_page'),

    path('add_student/', views.StudentView.add_student, name='add_student'),
]