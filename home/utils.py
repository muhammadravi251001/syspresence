from django.db.models.fields import EmailField
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status

def response(status = status.HTTP_200_OK, data = None, error = None):
    return Response({
					"data": data,
					"error": error,
				}, status=status)

def validateParams(request, params):
    for param in params:
        res = request.query_params.get(param)
        if res is None:
            return response(error="{} is required".format(param), status=status.HTTP_404_NOT_FOUND)
    return None
	
def validateBody(request, attrs):
    for attr in attrs:
        res = request.data.get(attr)
        if res is None:
            return response(error="{} is required".format(attr), status=status.HTTP_404_NOT_FOUND)
    return None
