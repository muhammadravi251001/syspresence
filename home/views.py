from django.shortcuts import redirect, render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from home.forms import *
from home.models import *
from rest_framework import permissions, status
from .utils import process_profile, response, validateBody, validateParams
from django.db import transaction
import logging
from home.serializers import *

logger = logging.getLogger(__name__)

class PageView():
    
    def landing_page(request):
        return render(request, 'home/landing_page.html')

    def add_course_page(request):
        return render(request, 'home/add_course_page.html')

    def all_course(request):
        all_course = Course.objects.all()
        context = {'course': all_course}
        return render(request, 'home/all_course_page.html', context=context)

    def add_student_page(request):
        all_course = Course.objects.all()
        context = {'course': all_course}
        return render(request, 'home/add_student_page.html', context=context)

class CourseView():

    def get_teacher(request):
        
        try:
            return Course.objects.get(teacher_name=str(request.user))
        
        except Exception:
            return None

    def get_student(request):
        
        try:
            return Course.objects.get(student_name=str(request.user))
        
        except Exception:
            return None
    
    def add_course(request):
        
        if request.method == 'POST':
            form = CourseForm()
            
            if form.is_valid:
                form = CourseForm(request.POST)
                form.save()
                return redirect('/all_course_page')
        
        else:
            return redirect('/add_course_page')

        return render(request, 'home/all_course_page.html')

    def update_course(request, pk):
        
        course = Course.objects.get(course_id=pk)
        form = CourseForm(instance=course)
        
        if request.method == 'POST':
            if form.is_valid():
                form = CourseForm(request.POST, instance=course)
                form.save()
                return redirect('/all_course_page')

        context = {'form': form}
        return render(request, 'home/update_course_page.html', context=context)

    def delete_course(request, pk):
        
        course = Course.objects.get(course_id=pk)
    
        if request.method == 'POST':
            course.delete()
            return redirect('/all_course_page')

        context = {'course': course}
        return render(request, 'home/delete_course_page.html', context=context)

    def detail_course(request, pk):
        
        course = Course.objects.filter(course_id=pk)
        students = Student.objects.filter(course_taken__in=course)
        
        context = {'students': students}
        return render(request, 'home/detail_course_page.html', context)

    @api_view(['GET', 'PUT', 'POST', 'DELETE'])
    def crud_course(request):
        
        """
        Handle CRUD of Course.
        Remember that this endpoint require Token Authorization. 
        """

        if request.method == 'GET':
            
            teacher = get_teacher(request)
            student = get_student(request)

            kelas = []
            
            if teacher is not None:
                #kelas = Course.objects.filter(daftarTeacher=teacher)
                
                #TODO: masih bingung apa yang mau difilter
                kelas = Course.objects.all()
            
            elif student is not None:
                #kelas = Course.objects.filter(isPublish=True,isVerified=True,daftarStudent=student)
                
                #TODO: masih bingung apa yang mau difilter
                kelas = Course.objects.all()
            
            if kelas.exists():
                return response(data=CourseSerializer(kelas, many=True).data)
            
            return response(data=[])

        if request.method == 'POST':
            
            user = get_teacher(request)
            
            if user is None:
                return response(error="You are not a teacher", status=status.HTTP_400_BAD_REQUEST)
            
            is_valid = validateBody(request, ['course_name', 'course_description'])
            
            if is_valid != None:
                return is_valid

            nama = request.data.get("course_name")
            deskripsi = request.data.get("course_description")

            try:
                with transaction.atomic():
                    review = Course.objects.create(
                        course_name=nama,
                        course_description=deskripsi,
                    )
                    
                    #TODO: masih bingung apa yang mau ditambah
                    #review.daftarTeacher.add(user)
            
            except Exception as e:
                logger.error(
                    "Failed to save review, request data {}".format(request.data))
                return response(error="Failed to save review, error message: {}".format(e), status=status.HTTP_404_NOT_FOUND)

            return response(data=CourseSerializer(review).data, status=status.HTTP_201_CREATED)

        if request.method == 'PUT':
            teacher = get_teacher(request)
            
            is_valid = validateBody(request, ['course_id', 'course_name', 'course_description'])
            
            if is_valid != None:
                return is_valid

            kelas_id = request.data.get("course_id")
            nama = request.data.get("course_name")
            deskripsi = request.data.get("course_description")

            #kelas = Course.objects.filter(daftarTeacher=teacher, id=kelas_id).first()
            
            #TODO: masih bingung apa yang mau difilter
            kelas = Course.objects.filter(course_id=kelas_id).first()
            
            if kelas is None:
                return response(error="Course does not exist", status=status.HTTP_409_CONFLICT)

            kelas.course_name = nama
            kelas.course_description = deskripsi
            kelas.save()

            return response(data=CourseSerializer(kelas).data, status=status.HTTP_201_CREATED)

        if request.method == 'DELETE':
            is_valid = validateParams(request, ['course_id'])
            
            if is_valid != None:
                return is_valid

            kelas_id = request.query_params.get("course_id")

            kelas = Course.objects.filter(course_id=kelas_id).first()
            
            if kelas is None:
                return response(error="Course does not exist", status=status.HTTP_409_CONFLICT)
            
            kelas.delete()
            
            return response(status=status.HTTP_200_OK)

class StudentView():
    
    #TODO: masih bingung kenapa ga bisa masuk valid ya?
    def add_student(request):
        
        if request.method == 'POST':
            print("MASUK IF")
            form = StudentForm(request.POST)
            
            if form.is_valid():
                print("MASUK IF VALID")
                form = StudentForm(request.POST)

                form.save()
                return redirect('/all_course_page')
            
            else:
                print(form.errors)
                print(form.non_field_errors)
                print(form.is_bound)
                print("MASUK IF terus ELSE")
        
        else:
            #print("MASUK ELSE")
            return redirect('/add_student_page')

        return render(request, 'home/all_course_page.html')

    '''
    def update_student(request, pk):
        
        student = Student.objects.get(course_id=pk)
        form = StudentForm(instance=student)
        
        if request.method == 'POST':
            if form.is_valid:
                form = StudentForm(request.POST, instance=student)
                form.save()
                return redirect('/all_course_page')

        context = {'form': form}
        return render(request, 'home/update_course_page.html', context=context)

    def delete_student(request, pk):
        
        student = Student.objects.get(course_id=pk)
    
        if request.method == 'POST':
            student.delete()
            return redirect('/all_course_page')

        context = {'student': student}
        return render(request, 'home/delete_course_page.html', context=context)
    '''